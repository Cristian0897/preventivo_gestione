package com.compito_finale.gestione_preventivo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.compito_finale.gestione_preventivo.models.Categoria;
import com.compito_finale.gestione_preventivo.services.CategoriaService;

@RestController
@RequestMapping("/categoria")
@CrossOrigin(origins="*", allowedHeaders="*")
public class CategoriaController {

	@Autowired
	private CategoriaService service ;
	
	@PostMapping("/inserisciCategoria")
	public Categoria saveCategoria( @RequestBody Categoria varCat ) {
		
		return service.inserisciCategoria(varCat) ;
		
	}
	
	@GetMapping("/")
	public List<Categoria> tutteLeCategorie(){
		
		return service.findAllCategorie() ;
		
	}
	
}
