package com.compito_finale.gestione_preventivo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.compito_finale.gestione_preventivo.models.Azienda;
import com.compito_finale.gestione_preventivo.services.AziendaService;

@RestController
@RequestMapping("/azienda")
@CrossOrigin(origins="*", allowedHeaders="*")
public class AziendaController {

	
	@Autowired
	private AziendaService service ;
	
	@GetMapping("/test")
	public String test() {
		
		return "Funziona Azienda" ;
		
	}
	
	@GetMapping("/")
	public List<Azienda> trovaAziende(){
		
		return service.findAll() ;
		
	}
	
}
