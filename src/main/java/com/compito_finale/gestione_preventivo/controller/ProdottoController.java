package com.compito_finale.gestione_preventivo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.compito_finale.gestione_preventivo.models.Prodotto;
import com.compito_finale.gestione_preventivo.services.CategoriaService;
import com.compito_finale.gestione_preventivo.services.ProdottoService;

@RestController
@RequestMapping("/prodotto")
@CrossOrigin(origins="*", allowedHeaders="*")
public class ProdottoController {
	
	@Autowired
	private ProdottoService service ;
	

	@GetMapping("/test")
	public String test() {
		
		return "Hi sei su prodotto" ;
		
	}
	
	@PostMapping("/insertProdotto")
	public Prodotto aggiungiProdotto(@RequestBody Prodotto varProd) {
		
		return service.saveProdotto(varProd) ;
		
	}
	
	@GetMapping("/")
	public List<Prodotto> tuttiProdotti(){
		
		return service.trovaProdotti() ;
		
	}
	
	

}
