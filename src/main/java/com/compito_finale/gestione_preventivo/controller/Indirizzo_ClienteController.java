package com.compito_finale.gestione_preventivo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.compito_finale.gestione_preventivo.models.Indirizzo_Cliente;
import com.compito_finale.gestione_preventivo.services.Indirizzo_ClienteService;

@RestController
@RequestMapping("/indirizzoCliente")
@CrossOrigin(origins="*", allowedHeaders="*")
public class Indirizzo_ClienteController {
	
	@Autowired
	private Indirizzo_ClienteService service ;
	
	@PostMapping("/insertIndirizzoCliente")
	public Indirizzo_Cliente aggiungiIndirizzoCliente( @RequestBody Indirizzo_Cliente objInd) {
		
		return service.saveindirizzoCliente(objInd) ;
		
	}
	
	@GetMapping("/")
	public List<Indirizzo_Cliente> tuttiGliIndirizzi(){
		
		return service.tuttiIndirizzi() ;
		
	}

}
