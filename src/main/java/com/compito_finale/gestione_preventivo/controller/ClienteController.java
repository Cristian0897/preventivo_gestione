package com.compito_finale.gestione_preventivo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.compito_finale.gestione_preventivo.models.Cliente;
import com.compito_finale.gestione_preventivo.services.ClienteService;


@RestController
@RequestMapping("/cliente")
@CrossOrigin(origins="*", allowedHeaders="*")
public class ClienteController {
	
	@Autowired
	private ClienteService service ;
	
	@PostMapping("/insertClient")
	public Cliente aggiungiCliente(@RequestBody Cliente varCli) {
		
		return service.saveCliente(varCli) ;
		
	} ;
	
	@GetMapping("/{cliente_id}")
	public Cliente dettaglio_cliente( @PathVariable int cliente_id ) {
		
		return service.findById(cliente_id) ;
		
	}
	
	@GetMapping("/")
	public List<Cliente> tutti_clienti(){
		
		return service.findAllClients() ;
		
	}
	
	@DeleteMapping("/{cliente_id}")
	public boolean elimina_cliente(@PathVariable int cliente_id) {
		
		return service.deleteCliente(cliente_id) ;
		
	}
	
	@PutMapping("/updateCliente")
	public boolean modifica_cliente(@RequestBody Cliente varCli) {
		
		return service.update(varCli) ;
		
	}

}
