package com.compito_finale.gestione_preventivo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.compito_finale.gestione_preventivo.models.Preventivo;
import com.compito_finale.gestione_preventivo.services.PreventivoService;

@RestController
@RequestMapping("/preventivo")
@CrossOrigin(origins="*", allowedHeaders="*")
public class PreventivoController {
	
	@Autowired
	private PreventivoService service ;
	
	@PostMapping("/insertPreventivo")
	public Preventivo aggiungiPreventivo( @RequestBody Preventivo varPre ) {
		
		return service.savePreventivo(varPre) ;
		
	}
	
	@GetMapping("/")
	public List<Preventivo> tutti_preventivi(){
		
		return service.tuttiPreventivi() ;
		
	}

}
