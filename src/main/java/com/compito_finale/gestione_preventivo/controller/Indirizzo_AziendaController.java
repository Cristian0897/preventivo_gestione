package com.compito_finale.gestione_preventivo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.compito_finale.gestione_preventivo.models.Indirizzo_Azienda;
import com.compito_finale.gestione_preventivo.services.Indirizzo_AziendaService;

@RestController
@RequestMapping("/indirizzo_azienda")
@CrossOrigin(origins="*", allowedHeaders="*")
public class Indirizzo_AziendaController {

	@Autowired
	private Indirizzo_AziendaService service ;
	
	@GetMapping("/test")
	public String test() {
		
		return "Funziona Azienda" ;
		
	}
	
	@GetMapping("/")
	public List<Indirizzo_Azienda> trovaIndirizzoAzienda(){
		
		return service.findAll() ;
		
	}
	
}
