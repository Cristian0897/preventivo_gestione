package com.compito_finale.gestione_preventivo.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table( name = "cliente_preventivo" )
public class Cliente {

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY)
	@Column( name = "id_cliente")
	private int id_cliente ;
	
	@Column( name = "intestatario_cliente")
	private String intestatario_cliente ;
	
	@OneToOne
	@JsonManagedReference
	@JoinColumn( name = "id_cliente" )
	private Indirizzo_Cliente indirizzo_cliente ;

	public Indirizzo_Cliente getIndirizzo_cliente() {
		return indirizzo_cliente;
	}

	public void setIndirizzo_cliente(Indirizzo_Cliente indirizzo_cliente) {
		this.indirizzo_cliente = indirizzo_cliente;
	}
	
	@OneToMany( mappedBy = "cliente_preventivo") 
	@JsonManagedReference
	private List<Preventivo> elencoPreventivi ;
	
	public List<Preventivo> getElencoPreventivi() {
		return elencoPreventivi;
	}

	public void setElencoPreventivi(List<Preventivo> elencoPreventivi) {
		this.elencoPreventivi = elencoPreventivi;
	}
	
	public int getId_cliente() {
		return id_cliente;
	}

	public void setId_cliente(int id_cliente) {
		this.id_cliente = id_cliente;
	}

	public String getIntestatario_cliente() {
		return intestatario_cliente;
	}

	public void setIntestatario_cliente(String intestatario_cliente) {
		this.intestatario_cliente = intestatario_cliente;
	}
	
}
