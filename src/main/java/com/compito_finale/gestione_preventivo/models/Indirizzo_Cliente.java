package com.compito_finale.gestione_preventivo.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table( name = "indirizzo_cliente" )
public class Indirizzo_Cliente {
	
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY)
	@Column( name = "id_indirizzo_cliente")
	private int id_indirizzo_cliente ;
	
	@Column( name = "via_indirizzo")
	private String via_indirizzo ;
	
	@Column( name = "citta_indirizzo")
	private String citta_indirizzo ;
	
	@Column( name = "cap_indirizzo")
	private int cap_indirizzo ;
	
	@Column( name = "provincia_indirizzo")
	private String provincia_indirizzo ;
	
	@OneToOne( mappedBy = "indirizzo_cliente")
	@JsonBackReference
	private Cliente cliente ;
	
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public int getId_indirizzo_cliente() {
		return id_indirizzo_cliente;
	}

	public void setId_indirizzo_cliente(int id_indirizzo_cliente) {
		this.id_indirizzo_cliente = id_indirizzo_cliente;
	}

	public String getVia_indirizzo() {
		return via_indirizzo;
	}

	public void setVia_indirizzo(String via_indirizzo) {
		this.via_indirizzo = via_indirizzo;
	}

	public String getCitta_indirizzo() {
		return citta_indirizzo;
	}

	public void setCitta_indirizzo(String citta_indirizzo) {
		this.citta_indirizzo = citta_indirizzo;
	}

	public int getCap_indirizzo() {
		return cap_indirizzo;
	}

	public void setCap_indirizzo(int cap_indirizzo) {
		this.cap_indirizzo = cap_indirizzo;
	}

	public String getProvincia_indirizzo() {
		return provincia_indirizzo;
	}

	public void setProvincia_indirizzo(String provincia_indirizzo) {
		this.provincia_indirizzo = provincia_indirizzo;
	}
	
	

}
