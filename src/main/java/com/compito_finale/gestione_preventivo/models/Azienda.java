package com.compito_finale.gestione_preventivo.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table( name = "azienda_preventivo" )
public class Azienda {
	
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY)
	@Column( name = "id_azienda_preventivo")
	private int id_azienda_preventivo ;
	
	@Column( name = "nome_azienda" )
	private String nome_azienda ;
	
	@OneToOne
	@JsonManagedReference
	@JoinColumn( name = "id_azienda_preventivo" )
	private Indirizzo_Azienda indirizzo ;

	public Indirizzo_Azienda getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(Indirizzo_Azienda indirizzo) {
		this.indirizzo = indirizzo;
	}

	public int getId_azienda_preventivo() {
		return id_azienda_preventivo;
	}

	public void setId_azienda_preventivo(int id_azienda_preventivo) {
		this.id_azienda_preventivo = id_azienda_preventivo;
	}

	public String getNome_azienda() {
		return nome_azienda;
	}

	public void setNome_azienda(String nome_azienda) {
		this.nome_azienda = nome_azienda;
	}
	
	

}
