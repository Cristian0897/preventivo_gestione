package com.compito_finale.gestione_preventivo.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table( name = "preventivo" )
public class Preventivo {
	
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY)
	@Column( name = "id_preventivo")
	private int id_preventivo ;
	
//	@Column( name = "cliente_rif" )
//	private int cliente_rif ;
	
	@Column( name = "codice_preventivo")
	private String codice_preventivo ;
	
	@ManyToOne
	@JsonBackReference
	@JoinColumn( name = "cliente_rif" )
	private Cliente cliente_preventivo ;
	
	
	@ManyToMany
	@JsonManagedReference
	@JoinTable(name = "preventivo_prodotto",
			joinColumns = { @JoinColumn(name="preventivo_rif", referencedColumnName = "id_preventivo") },
			inverseJoinColumns = { @JoinColumn(name="prodotto_rif", referencedColumnName = "id_prodotto") })
	private List<Prodotto> prodotti ; 

	public int getId_preventivo() {
		return id_preventivo;
	}

	public void setId_preventivo(int id_preventivo) {
		this.id_preventivo = id_preventivo;
	}

	public String getCodice_preventivo() {
		return codice_preventivo;
	}

	public void setCodice_preventivo(String codice_preventivo) {
		this.codice_preventivo = codice_preventivo;
	}

	public List<Prodotto> getProdotti() {
		return prodotti;
	}

	public void setProdotti(List<Prodotto> prodotti) {
		this.prodotti = prodotti;
	}

//	public int getCliente_rif() {
//		return cliente_rif;
//	}
//
//	public void setCliente_rif(int cliente_rif) {
//		this.cliente_rif = cliente_rif;
//	}

	public Cliente getCliente_preventivo() {
		return cliente_preventivo;
	}

	public void setCliente_preventivo(Cliente cliente_preventivo) {
		this.cliente_preventivo = cliente_preventivo;
	}
	
	
	
}
