package com.compito_finale.gestione_preventivo.models;

import javax.persistence.JoinColumn;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table( name = "prodotto" )
public class Prodotto {

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY)
	@Column( name = "id_prodotto")
	private int id_prodotto ;
	
	@Column( name = "codice_prodotto")
	private String codice_prodotto ;
	
	@Column( name = "nome_prodotto")
	private String nome_prodotto ;
	
	@Column( name = "quantita_prodotto")
	private int quantita_prodotto ;
	
	@Column( name = "prezzo_prodotto")
	private int prezzo_prodotto ;
	
	@Column( name = "totale_prodotto")
	private int totale_prodotto ;
	
	@ManyToMany
	@JsonManagedReference
	@JoinTable(name = "prodotto_categoria",
			joinColumns = { @JoinColumn(name="prodotto_categoria_rif", referencedColumnName = "id_prodotto") },
			inverseJoinColumns = { @JoinColumn(name="categoria_prodotto_rif", referencedColumnName = "id_categoria_prodotto") })
	private List<Categoria> lista_categoria ;
	
	@ManyToMany
	@JsonBackReference
	@JoinTable(name = "preventivo_prodotto",
			joinColumns = { @JoinColumn(name="prodotto_rif", referencedColumnName = "id_prodotto") },
			inverseJoinColumns = { @JoinColumn(name="preventivo_rif", referencedColumnName = "id_preventivo") })
	private List<Preventivo> preventivo ;
	
	public List<Categoria> getLista_categoria() {
		return lista_categoria;
	}

	public void setLista_categoria(List<Categoria> lista_categoria) {
		this.lista_categoria = lista_categoria;
	}
	
	public List<Preventivo> getPreventivo() {
		return preventivo;
	}

	public void setPreventivo(List<Preventivo> preventivo) {
		this.preventivo = preventivo;
	}

	public int getId_prodotto() {
		return id_prodotto;
	}

	public void setId_prodotto(int id_prodotto) {
		this.id_prodotto = id_prodotto;
	}

	public String getCodice_prodotto() {
		return codice_prodotto;
	}

	public void setCodice_prodotto(String codice_prodotto) {
		this.codice_prodotto = codice_prodotto;
	}

	public String getNome_prodotto() {
		return nome_prodotto;
	}

	public void setNome_prodotto(String nome_prodotto) {
		this.nome_prodotto = nome_prodotto;
	}

	public int getQuantita_prodotto() {
		return quantita_prodotto;
	}

	public void setQuantita_prodotto(int quantita_prodotto) {
		this.quantita_prodotto = quantita_prodotto;
	}

	public int getPrezzo_prodotto() {
		return prezzo_prodotto;
	}

	public void setPrezzo_prodotto(int prezzo_prodotto) {
		this.prezzo_prodotto = prezzo_prodotto;
	}

	public int getTotale_prodotto() {
		return totale_prodotto;
	}

	public void setTotale_prodotto(int totale_prodotto) {
		this.totale_prodotto = totale_prodotto;
	}
	
	
	
}
