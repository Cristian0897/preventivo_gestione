package com.compito_finale.gestione_preventivo.models;

import java.util.List;

import javax.annotation.Generated;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table( name = "categoria_prodotto" )
public class Categoria {
	
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY)
	@Column( name = "id_categoria_prodotto")
	private int id_categoria_prodotto ;
	
	@Column( name = "nome_categoria_prodotto" )
	private String nome_categoria_prodotto ;
		
	@ManyToMany
	@JsonBackReference
	@JoinTable(name = "prodotto_categoria",
			joinColumns = { @JoinColumn(name="categoria_prodotto_rif", referencedColumnName = "id_categoria_prodotto") },
			inverseJoinColumns = { @JoinColumn(name="prodotto_categoria_rif", referencedColumnName = "id_prodotto") })
	private List<Prodotto> lista_prodotti ;

	public List<Prodotto> getLista_prodotti() {
		return lista_prodotti;
	}

	public void setLista_prodotti(List<Prodotto> lista_prodotti) {
		this.lista_prodotti = lista_prodotti;
	}

	public int getId_categoria_prodotto() {
		return id_categoria_prodotto;
	}

	public void setId_categoria_prodotto(int id_categoria_prodotto) {
		this.id_categoria_prodotto = id_categoria_prodotto;
	}

	public String getNome_categoria_prodotto() {
		return nome_categoria_prodotto;
	}

	public void setNome_categoria_prodotto(String nome_categoria_prodotto) {
		this.nome_categoria_prodotto = nome_categoria_prodotto;
	}
	
}
