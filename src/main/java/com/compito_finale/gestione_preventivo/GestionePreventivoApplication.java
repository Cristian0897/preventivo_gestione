package com.compito_finale.gestione_preventivo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestionePreventivoApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestionePreventivoApplication.class, args);
	}

}
