package com.compito_finale.gestione_preventivo.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.compito_finale.gestione_preventivo.models.Cliente;

@Service
public class ClienteService {

	@Autowired
	private EntityManager entMan ;
	
	private Session getSessione() {
		
		return entMan.unwrap(Session.class) ;
		
	}
	
	public Cliente saveCliente( Cliente objCliente ) {
		
		Cliente temp = new Cliente() ;
		
		temp.setIntestatario_cliente(objCliente.getIntestatario_cliente());
		
		Session sessione = getSessione() ;
		
		try {
			
			sessione.save(temp) ;
			
		} catch( Exception e ) {
			
			System.out.println(e.getMessage());
			
		}
		
		return temp ;
		
		
	}
	
	public Cliente findById( int varId ) {
		
		Session sessione = getSessione() ;
		
		return( Cliente )sessione
				.createCriteria(Cliente.class)
				.add(Restrictions.eqOrIsNull("id_cliente", varId))
				.uniqueResult() ;
		
	}
	
	public List<Cliente> findAllClients(){
		
		Session sessione = getSessione() ;
		
		return sessione.createCriteria(Cliente.class).list() ;
		
	}
	
	@Transactional
	public boolean deleteCliente( int varId ) {
		
		Session sessione = getSessione() ;
		
		try {
			
			Cliente temp = sessione.load(Cliente.class, varId) ;
			
			sessione.delete(temp);
			sessione.flush();
			
			return true ;
			
		} catch( Exception e ) {
			
			System.out.println(e.getMessage());
			
		}
		
		return false ;
		
	}
	
	public boolean update( Cliente objCliente ) {
		
		Session sessione = getSessione() ;
		
		try {
			
			Cliente temp = sessione.load(Cliente.class, objCliente.getId_cliente()) ;
			
			if( temp != null ) {
				
				temp.setIntestatario_cliente(objCliente.getIntestatario_cliente());
				
				sessione.update(temp);
				sessione.flush() ;
				
				return true ;
				
			}
			
		} catch( Exception e ) {
			
			System.out.println(e.getMessage());
			
		}
		
		return false ;
		
	}
	
	
	
	
}
