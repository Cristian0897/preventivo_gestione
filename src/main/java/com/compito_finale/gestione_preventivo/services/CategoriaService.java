package com.compito_finale.gestione_preventivo.services;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.compito_finale.gestione_preventivo.models.Categoria;

@Service
public class CategoriaService {

	@Autowired
	private EntityManager entMan ;
	
	private Session getSessione() {
		
		return entMan.unwrap(Session.class) ;
		
	}
	
	public Categoria inserisciCategoria( Categoria objCat ) {
		
		Categoria temp = new Categoria() ;
		temp.setNome_categoria_prodotto(objCat.getNome_categoria_prodotto());
		
		
		Session sessione = getSessione() ;
		
		try {
			sessione.save(temp) ;
		} catch( Exception e ) {
			
			System.out.println(e.getMessage());
			
			return null ;
			
		}
		
		return temp ;
		
		
		
	}
	
	public Categoria findById( int varId ) {
		
		Session sessione = getSessione() ;
		
		return( Categoria )sessione
				.createCriteria(Categoria.class)
				.add(Restrictions.eqOrIsNull("id_categoria_prodotto", varId))
				.uniqueResult() ;
		
	}
	
	public List<Categoria> findAllCategorie(){
		
		Session sessione = getSessione() ;
		
		return sessione.createCriteria(Categoria.class).list() ;
		
	}
	
}
