package com.compito_finale.gestione_preventivo.services;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.compito_finale.gestione_preventivo.models.Prodotto;

@Service
public class ProdottoService {
	
	@Autowired
	private EntityManager entMan ;
	
	private Session getSessione() {
		
		return entMan.unwrap(Session.class) ;
		
	} ;
	
	public Prodotto saveProdotto( Prodotto objProd ) {
		
		Prodotto temp = new Prodotto() ;
		temp.setCodice_prodotto(objProd.getCodice_prodotto()) ;
		temp.setNome_prodotto(objProd.getNome_prodotto());
		temp.setQuantita_prodotto(objProd.getQuantita_prodotto());
		temp.setPrezzo_prodotto(objProd.getPrezzo_prodotto());
		temp.setTotale_prodotto(temp.getPrezzo_prodotto() * temp.getQuantita_prodotto());
		
		Session sessione = getSessione() ;
		
		try {
			
			sessione.save(temp) ;
			
		} catch( Exception e ) {
			
			System.out.println(e.getMessage());
			
		}
		
		return temp ;
		
	}
	
	public Prodotto trovaProdotto( int varId ) {
		
		Session sessione = getSessione() ;
		
		return( Prodotto )sessione
				.createCriteria(Prodotto.class)
				.add( Restrictions.eqOrIsNull("id_prodotto", varId))
				.uniqueResult() ;
		
	}
	
	public List<Prodotto> trovaProdotti(){
		
		Session sessione = getSessione() ;
		
		return sessione.createCriteria(Prodotto.class).list() ;
		
	}

}
