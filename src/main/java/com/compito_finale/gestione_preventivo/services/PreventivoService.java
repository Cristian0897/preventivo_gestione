package com.compito_finale.gestione_preventivo.services;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.compito_finale.gestione_preventivo.models.Preventivo;

@Service
public class PreventivoService {
	
	@Autowired
	private EntityManager entMan ;
	
	private Session getSessione() {
		
		return entMan.unwrap(Session.class) ;
		
	}
	
	public Preventivo savePreventivo( Preventivo objPrev ) {
		
		Preventivo temp = new Preventivo() ;
		temp.setCodice_preventivo(objPrev.getCodice_preventivo());
//		temp.setCliente_rif(objPrev.getCliente_rif());
		
		Session sessione = getSessione() ;
		
		try {
			
			sessione.save(temp) ;
			
		} catch( Exception e ) {
			
			System.out.println(e.getMessage());
			
		}
		
		return temp ;
		
	}
	
	public List<Preventivo> tuttiPreventivi(){
		
		Session sessione = getSessione() ;
		
		return sessione.createCriteria(Preventivo.class).list() ;
		
	}

}
