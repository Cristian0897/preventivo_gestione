package com.compito_finale.gestione_preventivo.services;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.compito_finale.gestione_preventivo.models.Indirizzo_Cliente;

@Service
public class Indirizzo_ClienteService {
	
	@Autowired
	private EntityManager entMan ;
	
	private Session getSessione() {
		
		return entMan.unwrap(Session.class) ;
		
	}
	
	public Indirizzo_Cliente saveindirizzoCliente( Indirizzo_Cliente objInd ) {
		
		Indirizzo_Cliente temp = new Indirizzo_Cliente() ;
		
		temp.setVia_indirizzo(objInd.getVia_indirizzo());
		temp.setCitta_indirizzo(objInd.getCitta_indirizzo());
		temp.setCap_indirizzo(objInd.getCap_indirizzo());
		temp.setProvincia_indirizzo(objInd.getProvincia_indirizzo());
		
		Session sessione = getSessione() ;
		
		try {
			
			sessione.save(temp) ;
			
		} catch( Exception e ) {
			
			System.out.println(e.getMessage());
			
		}
		
		return temp ;
		
	}
	
	public List<Indirizzo_Cliente> tuttiIndirizzi(){
		
		Session sessione = getSessione() ;
		
		return sessione.createCriteria(Indirizzo_Cliente.class).list() ;
		
	}
	
	

}
