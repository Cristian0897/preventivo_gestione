package com.compito_finale.gestione_preventivo.services;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.compito_finale.gestione_preventivo.models.Indirizzo_Azienda;

@Service
public class Indirizzo_AziendaService {
	
	@Autowired
	private EntityManager entMan ;
	
	private Session getSessione() {
		
		return entMan.unwrap(Session.class) ;
		
	}
	
	public List<Indirizzo_Azienda> findAll(){
		
		Session sessione = getSessione() ;
		
		return sessione.createCriteria(Indirizzo_Azienda.class).list() ;
		
	}

}
