package com.compito_finale.gestione_preventivo.services;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.compito_finale.gestione_preventivo.models.Azienda;

@Service
public class AziendaService {

	@Autowired
	private EntityManager entMan ;
	
	private Session getSessione() {
		
		return entMan.unwrap(Session.class) ;
		
	}
	
	public List<Azienda> findAll(){
		
		Session sessione = getSessione() ;
		
		return sessione.createCriteria(Azienda.class).list() ;
		
	}
	
}
