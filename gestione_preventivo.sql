DROP DATABASE IF EXISTS gestione_preventivo ;

CREATE DATABASE gestione_preventivo ;

USE gestione_preventivo ;

CREATE TABLE azienda_preventivo(

	id_azienda_preventivo INT NOT NULL AUTO_INCREMENT,
    
    nome_azienda VARCHAR(100) NOT NULL,
    
    PRIMARY KEY ( id_azienda_preventivo )
    
);

INSERT INTO azienda_preventivo( nome_azienda ) VALUE 
( "Square Enix" );

SELECT * FROM azienda_preventivo ;


CREATE TABLE indirizzo_azienda_preventivo(

	id_indirizzo_azienda INT NOT NULL AUTO_INCREMENT,
    
    via_indirizzo VARCHAR(100) NOT NULL,
    
    citta_indirizzo VARCHAR(100) NOT NULL,
    
    cap_indirizzo INT NOT NULL,
    
    provincia_indirizzo VARCHAR(10) NOT NULL,
    
    PRIMARY KEY ( id_indirizzo_azienda ),
    
    FOREIGN KEY ( id_indirizzo_azienda ) REFERENCES azienda_preventivo( id_azienda_preventivo )

);

INSERT INTO indirizzo_azienda_preventivo( via_indirizzo, citta_indirizzo, cap_indirizzo, provincia_indirizzo ) VALUE
("Shinjuku", "Tokyo", 1001211, "TO") ;

CREATE TABLE cliente_preventivo(

	id_cliente INT NOT NULL AUTO_INCREMENT,
    
    intestatario_cliente VARCHAR(100) NOT NULL,
    
    PRIMARY KEY ( id_cliente )
    
);

CREATE TABLE indirizzo_cliente(

	id_indirizzo_cliente INT NOT NULL AUTO_INCREMENT,
    
    via_indirizzo VARCHAR(100) NOT NULL,
    
    citta_indirizzo VARCHAR(100) NOT NULL,
    
    cap_indirizzo INT NOT NULL,
    
    provincia_indirizzo VARCHAR(10) NOT NULL,
    
    PRIMARY KEY ( id_indirizzo_cliente ),
    
    FOREIGN KEY ( id_indirizzo_cliente ) REFERENCES cliente_preventivo( id_cliente )

) ;

CREATE TABLE categoria_prodotto(

	id_categoria_prodotto INT NOT NULL AUTO_INCREMENT,
    
    nome_categoria_prodotto VARCHAR(100) NOT NULL,

	PRIMARY KEY ( id_categoria_prodotto )

);

INSERT INTO categoria_prodotto ( nome_categoria_prodotto ) VALUES
("Libri"),
("Musica, Film e TV" ),
("Videogiochi"),
("Elettronica e Informatica"),
("Casa, Giardino, Fai da Te "),
("Alimentari" ),
("Bellezza e Salute" );

SELECT * FROM categoria_prodotto;



CREATE TABLE  prodotto(

	id_prodotto INT NOT NULL AUTO_INCREMENT,
    
    codice_prodotto VARCHAR(100) NOT NULL,
    
    nome_prodotto VARCHAR(100) NOT NULL,
    
	quantità_prodotto INT NOT NULL,
    
    prezzo_prodotto INT NOT NULL,
    
    totale_prodotto INT NOT NULL,
    
    PRIMARY KEY ( id_prodotto ),
    
    FOREIGN KEY ( id_prodotto ) REFERENCES categoria_prodotto( id_categoria_prodotto ) 
    
) ;


CREATE TABLE preventivo(

	id_preventivo INT NOT NULL AUTO_INCREMENT ,
    
    codice_preventivo VARCHAR(100) NOT NULL,
    
    PRIMARY KEY ( id_preventivo ) 

);

CREATE TABLE preventivo_prodotto(

	prodotto_rif INT NOT NULL,
    
    preventivo_rif INT NOT NULL,
    
    FOREIGN KEY ( prodotto_rif ) REFERENCES prodotto(id_prodotto),
    
    FOREIGN KEY ( preventivo_rif ) REFERENCES preventivo( id_preventivo )

);

